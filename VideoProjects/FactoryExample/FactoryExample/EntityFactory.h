#pragma once

typedef enum EntityType {
	etWeapon,
	etLoot,
	etCharacter,

	etNumTypes
};

// Forward declaring EntityBase to allow me to have a pointer to EntityBase
// as the return value for CreateEntity.
class EntityBase;

class EntityFactory
{
public:
	EntityFactory();
	~EntityFactory();

	EntityBase* CreateEntity(EntityType type);
};

