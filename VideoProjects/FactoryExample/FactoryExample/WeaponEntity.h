#pragma once
#include "EntityBase.h"

class EntityFactory;

class WeaponEntity :
	public EntityBase
{
private:
	WeaponEntity();

public:
	virtual ~WeaponEntity();

	virtual void PrintIdentity();

	friend EntityFactory;
};

