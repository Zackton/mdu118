#include "stdafx.h"
#include "EntityFactory.h"
#include "EntityBase.h"

#include <vector>

int _tmain(int argc, _TCHAR* argv[])
{
	EntityFactory factory;

	// Create a standard vector containing each of the entities
	std::vector<EntityBase*> entities;
	entities.push_back(factory.CreateEntity(etLoot));
	entities.push_back(factory.CreateEntity(etCharacter));
	entities.push_back(factory.CreateEntity(etWeapon));

	// Tell all of the entities to identify themselves
	for (auto entityPtr : entities)
	{
		entityPtr->PrintIdentity();
	}

	// Free all of the allocated entities
	for (auto entityPtr : entities)
	{
		delete entityPtr;
	}

	return 0;
}

