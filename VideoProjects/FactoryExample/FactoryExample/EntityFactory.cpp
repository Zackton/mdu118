#include "stdafx.h"
#include "EntityFactory.h"

#include "LootEntity.h"
#include "WeaponEntity.h"
#include "CharacterEntity.h"

EntityFactory::EntityFactory()
{
}


EntityFactory::~EntityFactory()
{
}

EntityBase* EntityFactory::CreateEntity(EntityType type)
{
	EntityBase* entityPtr = nullptr;

	switch (type)
	{
	case etLoot:
		entityPtr = new LootEntity();
		break;

	case etWeapon:
		entityPtr = new WeaponEntity();
		break;

	case etCharacter:
		entityPtr = new CharacterEntity();
		break;
	}

	return entityPtr;
}
