#pragma once
class EntityBase
{
public:
	EntityBase();
	virtual ~EntityBase();

	virtual void PrintIdentity() = 0;
};

