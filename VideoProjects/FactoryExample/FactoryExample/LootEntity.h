#pragma once
#include "EntityBase.h"

class EntityFactory;

class LootEntity :
	public EntityBase
{
private:
	LootEntity();

public:
	virtual ~LootEntity();

	virtual void PrintIdentity();

	friend EntityFactory;
};

