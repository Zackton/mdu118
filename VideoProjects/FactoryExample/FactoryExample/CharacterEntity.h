#pragma once
#include "EntityBase.h"

class EntityFactory;

class CharacterEntity :
	public EntityBase
{
private:
	CharacterEntity();

public:
	virtual ~CharacterEntity();

	virtual void PrintIdentity();

	friend EntityFactory;
};

