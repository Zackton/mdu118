#include "stdafx.h"
#include <algorithm>
#include <iostream>
#include <vector>

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> intVector;

	// Add some arbitrary values into the vector
	intVector.push_back(1);
	intVector.push_back(5);
	intVector.push_back(-5);
	intVector.push_back(3);
	intVector.push_back(2);
	intVector.push_back(8);
	intVector.push_back(-10);

	{ // Bubble Sort Example
		std::vector<int> workingData = intVector;
		int iterationCount = 0;
		int swapCount = 0;

		bool didSwap = true;
		while (didSwap)
		{
			didSwap = false;

			// Iterating over all of the data
			for (std::vector<int>::iterator it = workingData.begin(); it != workingData.end(); ++it)
			{
				std::vector<int>::iterator currentIt = it;
				std::vector<int>::iterator nextIt = currentIt + 1;

				// If the next element is invalid then exit the loop
				if (nextIt == workingData.end())
					break;

				++iterationCount;

				// Are the values out of order? if so then swap
				if (*currentIt > *nextIt)
				{
					++swapCount;

					std::iter_swap(currentIt, nextIt);
					didSwap = true;
				}
			}
		}

		// Display the results
		std::cout << "Bubble sort results are:" << std::endl;
		for (auto value : workingData)
		{
			std::cout << "   " << value << std::endl;
		}
		std::cout << "Iterations: " << iterationCount << std::endl;
		std::cout << "Swap Count: " << swapCount << std::endl;
	}

	{ // Cocktail Example
		std::vector<int> workingData = intVector;
		int iterationCount = 0;
		int swapCount = 0;

		bool didSwap = true;
		while (didSwap)
		{
			didSwap = false;

			// Iterating over all of the data
			for (std::vector<int>::iterator it = workingData.begin(); it != workingData.end(); ++it)
			{
				std::vector<int>::iterator currentIt = it;
				std::vector<int>::iterator nextIt = currentIt + 1;

				// If the next element is invalid then exit the loop
				if (nextIt == workingData.end())
					break;

				++iterationCount;

				// Are the values out of order? if so then swap
				if (*currentIt > *nextIt)
				{
					++swapCount;

					std::iter_swap(currentIt, nextIt);
					didSwap = true;
				}
			}

			// Iterating over all of the data
			for (std::vector<int>::reverse_iterator it = workingData.rbegin(); it != workingData.rend(); ++it)
			{
				std::vector<int>::reverse_iterator currentIt = it;
				std::vector<int>::reverse_iterator prevIt = currentIt + 1;

				// If the previous element is invalid then exit the loop
				if (prevIt == workingData.rend())
					break;

				++iterationCount;

				// Are the values out of order? if so then swap
				if (*prevIt > *currentIt)
				{
					++swapCount;

					std::iter_swap(currentIt, prevIt);
					didSwap = true;
				}
			}
		}

		// Display the results
		std::cout << "Cocktail sort results are:" << std::endl;
		for (auto value : workingData)
		{
			std::cout << "   " << value << std::endl;
		}
		std::cout << "Iterations: " << iterationCount << std::endl;
		std::cout << "Swap Count: " << swapCount << std::endl;
	}

	{ // Built in sort
		std::vector<int> workingData = intVector;
		
		// Use the built in sort with a lambda function to sort the data in descending order
		std::sort(workingData.begin(), workingData.end(), [](const int& lhs, const int& rhs) {
			return lhs > rhs;
		});

		// Display the results
		std::cout << "Built in sort results are:" << std::endl;
		for (auto value : workingData)
		{
			std::cout << "   " << value << std::endl;
		}
	}

	return 0;
}

