// W3_DemoApp2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// Each of the values in this enumeration will be numbered automatically
typedef enum AIState {
	eaisIdle,			// Value will be 0
	eaisSearching,
	eaisAttacking,
	eaisDeceased,

	eaisNumStates		// Value will be 4 - can be used to know how many entries are in the enumeration
};

// Every value in this enumeration hs been manually set
typedef enum AIFlags {
	eaifVisible		= 0x01,
	eaifMoving		= 0x02,
	eaifAggressive	= 0x04
};

int _tmain(int argc, _TCHAR* argv[])
{
	AIState currentState = eaisIdle;
	int currentFlags = eaifVisible | eaifAggressive;

	if (currentFlags & eaifVisible)
	{
		// AI is visible
	}
	else
	{
		// AI is not visible
	}

	const int MaxNumberOfAIs = 32;

	return 0;
}

