#pragma once

#include "BaseObservee.h"
#include "BaseObserver.h"

class Character : public BaseObservee, public BaseObserver
{
public:
	Character(const std::string& _name);
	virtual ~Character();

	virtual void SawEnemy(const BaseObservee* observeePtr);
	virtual void AttackedByEnemy(const BaseObservee* observeePtr);
	virtual void KilledByEnemy(const BaseObservee* observeePtr);
};

