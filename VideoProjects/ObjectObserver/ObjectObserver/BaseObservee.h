#pragma once

#include <list>
#include <string>

class BaseObserver;

class BaseObservee
{
public:
	BaseObservee(const std::string& _name);
	virtual ~BaseObservee();

	void RegisterObserver(BaseObserver* observerPtr);
	void UnregisterObserver(BaseObserver* observerPtr);

	const std::string& Name() const { return name; }

//protected:
	void Notify_SawEnemy();
	void Notify_AttackedByEnemy();
	void Notify_KilledByEnemy();

private:
	std::list<BaseObserver*> observers;
	std::string name;
};

