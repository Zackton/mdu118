#pragma once

class BaseObservee;

class BaseObserver
{
public:
	BaseObserver();
	virtual ~BaseObserver();

	virtual void SawEnemy(const BaseObservee* observeePtr) = 0;
	virtual void AttackedByEnemy(const BaseObservee* observeePtr) = 0;
	virtual void KilledByEnemy(const BaseObservee* observeePtr) = 0;
};

