#include "stdafx.h"
#include "Character.h"
#include <iostream>

Character::Character(const std::string& _name) :
BaseObservee(_name)
{
}


Character::~Character()
{
}

void Character::SawEnemy(const BaseObservee* observeePtr)
{
	std::cout << Name() << " was told that " << observeePtr->Name() << " saw an enemy" << std::endl;
}

void Character::AttackedByEnemy(const BaseObservee* observeePtr)
{
	std::cout << Name() << " was told that " << observeePtr->Name() << " was attacked by an enemy" << std::endl;
}

void Character::KilledByEnemy(const BaseObservee* observeePtr)
{
	std::cout << Name() << " was told that " << observeePtr->Name() << " was killed by enemy" << std::endl;
}
