#include "stdafx.h"
#include "Character.h"

int _tmain(int argc, _TCHAR* argv[])
{
	Character player("Player");
	Character companionAI("CompanionAI");

	// Set each character as an observer of the other
	player.RegisterObserver(&companionAI);
	companionAI.RegisterObserver(&player);

	// Trigger some of the events
	player.Notify_SawEnemy();
	companionAI.Notify_AttackedByEnemy();

	// Remove the observers
	player.UnregisterObserver(&companionAI);
	companionAI.UnregisterObserver(&player);

	return 0;
}

