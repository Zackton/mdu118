#include "stdafx.h"
#include "BaseObservee.h"
#include "BaseObserver.h"

#include <algorithm>

BaseObservee::BaseObservee(const std::string& _name) :
	name(_name)
{
}


BaseObservee::~BaseObservee()
{
}

void BaseObservee::RegisterObserver(BaseObserver* observerPtr)
{
	observers.push_back(observerPtr);
}

void BaseObservee::UnregisterObserver(BaseObserver* observerPtr)
{
	auto eraseStartPoint = std::remove_if(observers.begin(), observers.end(), [observerPtr](BaseObserver* currentObserverPtr){
		return currentObserverPtr == observerPtr;
	});

	observers.erase(eraseStartPoint, observers.end());
}

void BaseObservee::Notify_SawEnemy()
{
	for (auto observer : observers)
	{
		observer->SawEnemy(this);
	}
}

void BaseObservee::Notify_AttackedByEnemy()
{
	for (auto observer : observers)
	{
		observer->AttackedByEnemy(this);
	}
}

void BaseObservee::Notify_KilledByEnemy()
{
	for (auto observer : observers)
	{
		observer->KilledByEnemy(this);
	}
}
