#include "stdafx.h"
#include <string>
#include <iostream>

class BaseClass1
{
public:
	virtual std::string Identity() const { return "BaseClass1"; }
};

class BaseClass2
{
public:
	virtual std::string Identity() const { return "BaseClass2"; }
};

class ChildOfBaseClass1 : public BaseClass1
{
public:
	virtual std::string Identity() const { return "ChildOfBaseClass1"; }
};

class ChildOfBaseClass2 : public BaseClass2
{
public:
	virtual std::string Identity() const { return "ChildOfBaseClass2"; }
};

int _tmain(int argc, _TCHAR* argv[])
{
	BaseClass1* baseClass1Ptr = new BaseClass1();
	BaseClass2* baseClass2Ptr = new BaseClass2();
	ChildOfBaseClass1* childOfBaseClass1Ptr = new ChildOfBaseClass1();
	ChildOfBaseClass2* childOfBaseClass2Ptr = new ChildOfBaseClass2();

	{ // static cast example
		//   - static_cast performs an implicit conversion between types (eg. int to float etc)
		//   - static_cast will call implicit conversion functions (eg. if you have 
		//     defined a method to convert your class to an int)
		//   - static_cast can cast through inheritance hierarchies but it does not perform
		//     any type checking to ensure the cast is valid

		int intValue = static_cast<int>(1.5f);

		BaseClass1* ptr1 = static_cast<BaseClass1*>(childOfBaseClass1Ptr);
		ChildOfBaseClass1* ptr2 = static_cast<ChildOfBaseClass1*>(ptr1);
	}

	{ // dynamic cast example
		//   - dynamic_cast is primarily used when working with polymorphic objects
		//   - dynamic_cast performs type checking and will return null/nullptr if
		//     the cast is invalid (eg. you cast something to an unrelated class)

		BaseClass1* ptr1 = dynamic_cast<BaseClass1*>(childOfBaseClass1Ptr);
		ChildOfBaseClass1* ptr2 = dynamic_cast<ChildOfBaseClass1*>(ptr1);

		BaseClass2* ptr3 = dynamic_cast<BaseClass2*>(childOfBaseClass1Ptr);

		std::cout << "Finished dynamic cast" << std::endl;
	}

	{ // const cast example
		//   - const_cast allows you to add or remove const from a variable

		const BaseClass1* constPtr2 = baseClass1Ptr;

		BaseClass1* ptr3 = const_cast<BaseClass1*>(constPtr2);
	}

	{ // reinterpret cast
		//   - reinterpret_cast allows you to cast to any variable type
		//   - reinterpret_cast does NOT perform any validity checking and if used
		//     incorrectly can cause instability

		// This code will compile and run. But the resulting ptr1 is absolute gibberish and unsafe to use
		std::string exampleString = "This is a test!";
		float* ptr1 = reinterpret_cast<float*>(&exampleString);

		// This code will also compile and run. But the resulting ptr2 is absolute gibberish and unsafe to use
		float floatValue = 123.45f;
		std::string* ptr2 = reinterpret_cast<std::string*>(&floatValue);
	}

	{ // C style cast
		// Because C++ is a superset of C you can also still use the old style C cast.
		// Under the hood the old style C will try the following, in order, 
		// and use the first that succeeds:
		//  - const_cast
		//  - static_cast
		//  - static_cast combined with a const_cast
		//  - reinterpret_cast
		//  - reinterpret_cast combined with a const_cast
		// Because the old style C cast can become a reinterpret_cast it is recommended
		// that you use the specific C++ casts to make the intended behaviour explicit.

		int intValue = (int)(1.5f);

		// This code will compile and run. But the resulting ptr1 is absolute gibberish and unsafe to use
		std::string exampleString = "This is a test!";
		float* ptr1 = (float*)(&exampleString);

		// This code will also compile and run. But the resulting ptr2 is absolute gibberish and unsafe to use
		float floatValue = 123.45f;
		std::string* ptr2 = (std::string*)(&floatValue);
	}

	delete baseClass1Ptr;
	delete baseClass2Ptr;
	delete childOfBaseClass1Ptr;
	delete childOfBaseClass2Ptr;

	return 0;
}

