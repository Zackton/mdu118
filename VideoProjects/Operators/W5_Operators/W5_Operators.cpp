#include "stdafx.h"

#include <iostream>
#include <string>

class Vector3
{
public:
	Vector3(float _x = 0.0f, float _y = 0.0f, float _z = 0.0f) :
		x(_x),
		y(_y),
		z(_z)
	{
		std::cout << "Called constructor" << std::endl;
	}

	void operator = (const Vector3& rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;

		std::cout << "Assignment operator called" << std::endl;
	}

	bool operator == (const Vector3& rhs)
	{
		return x == rhs.x && y == rhs.y && z == rhs.z;
	}

	bool operator != (const Vector3& rhs)
	{
		return !(*this == rhs);
	}

	std::ostream& operator << (std::ostream& os)
	{
		return os << x << "," <<  y << "," <<  z;
	}

public:
	float x, y, z;
};

int _tmain(int argc, _TCHAR* argv[])
{
	Vector3 testVector = Vector3(3.4f, 1.2f, 5.6f);

	testVector = Vector3(3.4f, 1.2f, 5.6f);

	testVector << std::cout;

	//std::cout << testVector << std::endl;

	return 0;
}

