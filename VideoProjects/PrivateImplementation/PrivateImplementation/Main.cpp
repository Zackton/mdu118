#include "stdafx.h"
#include "PublicFacingClass.h"

#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	PublicFacingClass publicFacingClass;

	std::cout << publicFacingClass.ExampleWrapper(1) << std::endl;
	std::cout << publicFacingClass.ExampleWrapper(4) << std::endl;

	return 0;
}

