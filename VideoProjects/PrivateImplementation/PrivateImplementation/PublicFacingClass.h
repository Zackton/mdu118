#pragma once

#include <string>

// Forward declaration for PrivateImplementation class
// This indicates to the compiler/linker that something called
// PrivateImplementation will exist and it will be a class.
class PrivateImplementation;

class PublicFacingClass
{
public:
	PublicFacingClass();
	~PublicFacingClass();

	std::string ExampleWrapper(int intValue);

private:
	PrivateImplementation* pimplPtr;
};

