#include "stdafx.h"
#include "PublicFacingClass.h"

////////////////////////////////////////////////////////////////////////////////
// Begin PrivateImplementation Class

class PrivateImplementation
{
public:
	std::string EvenMethod(int intValue);
	std::string OddMethod(int intValue);
};

std::string PrivateImplementation::EvenMethod(int intValue)
{
	return std::to_string(intValue) + " is even";
}

std::string PrivateImplementation::OddMethod(int intValue)
{
	return std::to_string(intValue) + " is odd";
}

// End PrivateImplementation Class
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Begin PublicFacingClass Implementation

PublicFacingClass::PublicFacingClass()
{
	// Instantiate our private implementation class
	pimplPtr = new PrivateImplementation();
}

PublicFacingClass::~PublicFacingClass()
{
	// Cleanup the private implementation class
	delete pimplPtr;
}

std::string PublicFacingClass::ExampleWrapper(int intValue)
{
	// Based on the input value call a different method within the private implementation
	if ((intValue % 2) == 0)
		return pimplPtr->EvenMethod(intValue);
	else
		return pimplPtr->OddMethod(intValue);
}

// End PublicFacingClass Implementation
////////////////////////////////////////////////////////////////////////////////
