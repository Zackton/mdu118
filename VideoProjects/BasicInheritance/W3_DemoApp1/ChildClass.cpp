#include "stdafx.h"

#include "ChildClass.h"

#include <iostream>

ChildClass::ChildClass(const std::string& _name, const std::string& _alternativeName) :
ParentClass(_name), // Calling the constructor of the ParentClass and pass through the name
alternativeName(_alternativeName)
{

}

ChildClass::~ChildClass()
{

}

void ChildClass::PrintIdentity() const
{
	// Call the PrintIdentity method of the parent class
	ParentClass::PrintIdentity();

	std::cout << "The class has the alternate name " << alternativeName << std::endl;
}

void ChildClass::MandatoryFunction() const
{

}
