#include "stdafx.h"

#include "ParentClass.h"

#include <iostream>

ParentClass::ParentClass(const std::string& _name) :
name(_name)
{

}

ParentClass::~ParentClass()
{

}

void ParentClass::PrintIdentity() const
{
	std::cout << "The class is named " << name << std::endl;
}
