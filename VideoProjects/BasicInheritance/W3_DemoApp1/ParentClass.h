// These two lines below are known as a header guard. They prevent multiple copies
// of the same header file being included. The two lines pair with the last
// line of the file (#endif).
#ifndef PARENT_CLASS_H
#define PARENT_CLASS_H

#include <string>

class ParentClass
{
public:
	ParentClass(const std::string& _name);

	virtual ~ParentClass();

	virtual void PrintIdentity() const;

	// Create a pure virtual function by using the virtual keyword and adding = 0
	// A pure virtual function MUST be implemented by a child class.
	// A class that has pure virtual functions cannot be instantiated.
	// eg. ParentClass parentClass; will trigger a compile error
	virtual void MandatoryFunction() const = 0;

protected:
	std::string name;
};

#endif // PARENT_CLASS_H
