#ifndef CHILD_CLASS_H
#define CHILD_CLASS_H

#include <string>

#include "ParentClass.h"

class ChildClass : public ParentClass
{
public:
	ChildClass(const std::string& _name, const std::string& _alternativeName);

	virtual ~ChildClass();

	virtual void PrintIdentity() const;

	virtual void MandatoryFunction() const;

protected:
	std::string alternativeName;
};

#endif // CHILD_CLASS_H