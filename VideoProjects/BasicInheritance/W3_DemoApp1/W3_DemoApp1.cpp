#include "stdafx.h"
#include <string>
#include <iostream>

#include "ParentClass.h"
#include "ChildClass.h"

int _tmain(int argc, _TCHAR* argv[])
{
	{
		ParentClass* parentPtr = new ChildClass("ChildClass1", "AlternativeName1");

		parentPtr->PrintIdentity();

		delete parentPtr;
	}

	return 0;
}

