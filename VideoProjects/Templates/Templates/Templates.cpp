#include "stdafx.h"
#include "Vector3.h"

// It is also possible to template a single function (either a standalone one or one inside of a class)
template <typename T>
T AddValues(const T& val1, const T& val2)
{
	return val1 + val2;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Vector3<int> intVector;
	Vector3i intVector2;
	Vector3<float> floatVector;
	Vector3f floatVector2;

	int sum = AddValues(1, 3);
	float floatSum = AddValues(1.1f, 3.1f);

	return 0;
}

