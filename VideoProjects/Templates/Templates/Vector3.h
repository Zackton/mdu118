#ifndef __VECTOR_3_H
#define __VECTOR_3_H

// Declare a vector 3 class that is templated on the variable type T
template <typename T>
class Vector3
{
public:
	Vector3(T _x = 0, T _y = 0, T _z = 0);

	bool operator == (const Vector3<T>& rhs) const;

public:
	T x;
	T y;
	T z;
};

// Use typedef to create easy to acces integer and float versions of our templated class
typedef Vector3<int> Vector3i;
typedef Vector3<float> Vector3f;

// When defining the implementation of a templated function outside of the class
// it is necessary to specifically indicate that it is templated.
template <typename T>
Vector3<T>::Vector3(T _x, T _y, T _z) :
x(_x),
y(_y),
z(_z)
{

}

template <typename T>
bool Vector3<T>::operator == (const Vector3<T>& rhs) const
{
	return (x == rhs.x) && (y == rhs.y) && (z == rhs.z);
}

#endif // __VECTOR_3_H