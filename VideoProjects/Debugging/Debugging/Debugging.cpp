#include "stdafx.h"
#include <iostream>

void DoSomething(int& value)
{
	value *= value;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int value = 42;
	int* valuePtr = &value;

	for (int index = 0; index < 10; ++index)
	{
		DoSomething(value);
	}

	std::cout << value << std::endl;

	return 0;
}

