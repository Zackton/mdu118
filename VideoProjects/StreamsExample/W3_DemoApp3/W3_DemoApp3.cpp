#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

struct Vector2
{
	Vector2(float _x = 0, float _y = 0) :
		x(_x),
		y(_y)
	{

	}

	float x, y;
};

// Example stream output operator for Vector2
std::ostream& operator << (std::ostream& os, const Vector2& vector)
{
	return os << "(" << vector.x << "," << vector.y << ")";
}

// Example of stream input operator for Vector2
std::istream& operator >> (std::istream& is, Vector2& vector)
{
	//        (           X           ,           Y          )
	// [unsigned char] [float] [unsigned char] [float] [unsigned char]

	unsigned char dummyChar;

	return is >> dummyChar 
			  >> vector.x 
			  >> dummyChar 
			  >> vector.y 
			  >> dummyChar;
}

int _tmain(int argc, _TCHAR* argv[])
{
	{
		Vector2 myVector(1, 2);

		std::cout << "myVector2 is " << myVector << std::endl;
	}

	{ // Example of writing to a text file
		std::ofstream fileStream("VectorList.txt", std::ios::app);

		Vector2 myVector(1, 2);

		std::ostringstream outputStream;

		// Insert the location and vector into the string stream
		outputStream << "Location" << "," << myVector;

		// Output the string stream to file
		fileStream << outputStream.str() << std::endl;
	}

	{ // Example of reading from a text file
		std::ifstream fileStream("VectorList.txt");

		// Read in all of the lines from the file
		std::string fileLine;
		while (fileStream >> fileLine)
		{
			// Passing the file line to inputStream so we can deconstruct it
			std::istringstream inputStream(fileLine);

			// Read up to the first comma and store that in vectorName
			std::string vectorName;
			std::getline(inputStream, vectorName, ',');

			// Read in our vector
			Vector2 myVector;
			inputStream >> myVector;

			std::cout << "Read in " << myVector << " from text file" << std::endl;
		}
	}

	{ // Example of writing to a binary file
		std::ofstream fileStream("VectorList.bin", std::ios::binary);

		Vector2 myVector(1, 2);

		fileStream.write(reinterpret_cast<char*>(&myVector), sizeof(myVector));
	}

	{ // Example of reading from a binary file
		std::ifstream fileStream("VectorList.bin", std::ios::binary);

		Vector2 myVector;

		fileStream.read(reinterpret_cast<char*>(&myVector), sizeof(myVector));

		std::cout << "myVector read from binary file is " << myVector << std::endl;
	}

	{
		std::string personsName;

		// Read in the person's name using std::cin
		std::cout << "What is your name?" << std::endl;
		std::cin >> personsName;

		// Read in the random value using std::cin
		std::cout << "Enter in a random number" << std::endl;
		int randomValue;
		std::cin >> randomValue;

		std::cout << "Hello " << personsName << " the meaning of life is " << randomValue << std::endl;
	}

	return 0;
}

