#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>

int _tmain(int argc, _TCHAR* argv[])
{
	{ // String Output Stream - Used to construct a string
		std::ostringstream stream;

		stream << "The number is " << "43";

		std::string streamValue = stream.str();
		std::cout << streamValue << std::endl;
	}

	{ // String Input Stream - Used to deconstruct a string
		std::string inputString = "1,2,3,4,5";
		std::istringstream inputStream(inputString);

		// Continue reading from the stream as long as the data is good
		while (inputStream.good())
		{
			int intValue;
			inputStream >> intValue;

			// Read in the dummy character if the stream still has data
			if (inputStream.good())
			{
				char dummyChar;
				inputStream >> dummyChar;
			}

			std::cout << "Read in " << intValue << std::endl;
		}
	}

	{ // Advanced String Input Stream
		std::string inputString = "Hello World,1,3.14";

		// Setup an input stream with inputString as the source data
		std::istringstream inputStream(inputString);

		std::string message;
		std::getline(inputStream, message, ',');

		int intValue = 0;
		inputStream >> intValue;

		char dummyChar;
		inputStream >> dummyChar;

		float floatValue = 0;
		inputStream >> floatValue;

		std::cout << "Message   : " << message << std::endl;
		std::cout << "IntValue  : " << intValue << std::endl;
		std::cout << "FloatValue: " << floatValue << std::endl;
	}

	return 0;
}

