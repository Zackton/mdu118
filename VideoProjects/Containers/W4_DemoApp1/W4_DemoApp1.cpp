#include "stdafx.h"
#include <vector>
#include <list>
#include <map>

#include <iostream>
#include <string>

int _tmain(int argc, _TCHAR* argv[])
{
	{ // std::vector Example
		std::vector<int> integerVector;

		// Reserve enough space for 4 elements
		integerVector.reserve(4);

		// Insert 4 elements
		integerVector.push_back(2);
		integerVector.push_back(4);
		integerVector.push_back(6);
		integerVector.push_back(8);

		// Output second element
		std::cout << integerVector[1] << std::endl;

		// Loop through vector based on index
		for (int index = 0; index < integerVector.size(); ++index)
		{
			std::cout << "Element " << index << " is " << integerVector[index] << std::endl;
		}

		// Loop through the vector using iterators
		for (std::vector<int>::iterator vectorIt = integerVector.begin(); vectorIt != integerVector.end(); ++vectorIt)
		{
			int& intRef = *vectorIt;

			std::cout << "Element " << std::distance(integerVector.begin(), vectorIt) << " is " << intRef << std::endl;
		}

		// Loop through the vector in reverse using iterators
		for (std::vector<int>::reverse_iterator vectorIt = integerVector.rbegin(); vectorIt != integerVector.rend(); ++vectorIt)
		{
			int& intRef = *vectorIt;

			std::cout << "Element " << std::distance(integerVector.rbegin(), vectorIt) << " is " << intRef << std::endl;
		}
	}

	{ // std::list Example
		std::list<int> integerList;

		// Insert 4 elements
		integerList.push_back(4);
		integerList.push_back(6);
		integerList.push_back(8);

		integerList.push_front(2);

		// Loop through the list using iterators
		for (std::list<int>::iterator listIt = integerList.begin(); listIt != integerList.end(); ++listIt)
		{
			int& intRef = *listIt;

			std::cout << "Element " << std::distance(integerList.begin(), listIt) << " is " << intRef << std::endl;
		}

		// Loop through the list in reverse using iterators
		for (std::list<int>::reverse_iterator listIt = integerList.rbegin(); listIt != integerList.rend(); ++listIt)
		{
			int& intRef = *listIt;

			std::cout << "Element " << std::distance(integerList.rbegin(), listIt) << " is " << intRef << std::endl;
		}
	}

	{ // std::map Example
		typedef std::map<int, std::string> NumberNameMap;
		typedef NumberNameMap::iterator NumberNameMapIter;

		NumberNameMap numberNameLookup;

		// Store data to the name lookup
		numberNameLookup[0] = "Zero";
		numberNameLookup[1] = "One";
		numberNameLookup[2] = "Two";
		numberNameLookup[3] = "Three";

		// Output the value associated with the key (0)
		std::cout << "0 is " << numberNameLookup[0] << std::endl;

		// Search in the map for the key (3)
		NumberNameMapIter nameIter = numberNameLookup.find(3);

		// Has the entry been found?
		if (nameIter != numberNameLookup.end())
		{
			std::cout << "3 is " << nameIter->second << std::endl;
		}
	}

	return 0;
}

