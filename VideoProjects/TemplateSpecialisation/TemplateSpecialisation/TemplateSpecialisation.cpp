#include "stdafx.h"
#include <iostream>

// Setup the generic template with a single method.
template <typename T>
class ExampleTemplate
{
public:
	void SpecialisedMethod(const T& input);
};

// Implement the generic version of the SpecialisedMethod function
template <typename T>
void ExampleTemplate<T>::SpecialisedMethod(const T& input)
{
	std::cout << "Called generic version of SpecialisedMethod with " << input << std::endl;
}

// Implement a specialisation of the method based on an int
void ExampleTemplate<int>::SpecialisedMethod(const int& input)
{
	std::cout << "Called int version of SpecialisedMethod with " << input << std::endl;
}

// Implement a specialisation of the method based on a double
void ExampleTemplate<double>::SpecialisedMethod(const double& input)
{
	std::cout << "Called double version of SpecialisedMethod with " << input << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	ExampleTemplate<int> intExample;
	ExampleTemplate<double> doubleExample;

	intExample.SpecialisedMethod(5);

	doubleExample.SpecialisedMethod(1.234);

	return 0;
}

