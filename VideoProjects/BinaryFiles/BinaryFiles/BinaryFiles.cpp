#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

int _tmain(int argc, _TCHAR* argv[])
{
	{ // File Output Stream - Used to write data to a file
		std::ofstream stream("TestData.bin", std::ios::binary);

		std::string message = "Hello World";
		int integerValues[5] = { 1, 2, 3, 4, 5 };

		// Write the message size and the message
		size_t messageSize = message.length();
		stream.write(reinterpret_cast<char*>(&messageSize), sizeof(size_t));
		stream.write(message.c_str(), message.length());

		// Write the array size and the array data
		size_t numIntegers = sizeof(integerValues) / sizeof(int);
		stream.write(reinterpret_cast<char*>(&numIntegers), sizeof(size_t));
		stream.write(reinterpret_cast<char*>(integerValues), sizeof(integerValues));
	}

	{ // File Input Stream - Used to read data from a file
		std::ifstream inputStream("TestData.bin", std::ios::binary);

		// Read in the message size
		size_t messageSize = 0;
		inputStream.read(reinterpret_cast<char*>(&messageSize), sizeof(size_t));

		// Setup a buffer for the message
		char* messageBuffer = new char[messageSize + 1];

		// Read in the message to the buffer
		inputStream.read(messageBuffer, messageSize);

		// Add the end of string character to the buffer
		messageBuffer[messageSize] = '\0';

		// Convert the message buffer to a string
		std::string message(messageBuffer);

		// Free up the message buffer
		delete[] messageBuffer;

		// Read in the integer array size
		size_t numIntegers = 0;
		inputStream.read(reinterpret_cast<char*>(&numIntegers), sizeof(size_t));

		// Allocate an array for the integers
		int* integerValues = new int[numIntegers];

		// Read the integer array from the file
		inputStream.read(reinterpret_cast<char*>(integerValues), sizeof(int) * numIntegers);

		// Display the data
		std::cout << message << std::endl;
		for (int index = 0; index < numIntegers; ++index)
		{
			std::cout << integerValues[index] << std::endl;
		}

		// Free the integer array
		delete[] integerValues;
	}

	return 0;
}