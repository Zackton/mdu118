#include "stdafx.h"
#include <iostream>

class ExampleSingleton
{
public:
	// Marking this function as static means that it belongs to the class
	// not an instance of the class. This function serves as the access point
	// for the singleton.
	static ExampleSingleton& Instance()
	{
		// By marking this variable as static we are saying that it must not
		// be thrown away when the function exits. It's value will be preserved
		// between calls of the function. The ExampleSingleton instance (_instance)
		// will only be created the first time this function is run. All subsequent
		// calls will skip the line below.
		static ExampleSingleton _instance;

		return _instance;
	}

	void SayHello()
	{
		std::cout << "Hello! I'm a singleton!" << std::endl;
	}

private:
	// Make the constructor private to prevent anyone else
	// creating an instance of the singleton.
	ExampleSingleton()
	{
	}
};

// Define a macro for the singleton's instance method
#define ExampleSingletonInstance ExampleSingleton::Instance()

int _tmain(int argc, _TCHAR* argv[])
{
	ExampleSingletonInstance.SayHello();

	return 0;
}

