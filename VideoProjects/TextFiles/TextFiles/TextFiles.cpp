#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

int _tmain(int argc, _TCHAR* argv[])
{
	{ // File Output Stream - Used to write data to a file
		std::ofstream stream("TestData.csv");

		stream << "Hello World,1,2,3,4,5" << std::endl;
		stream << "Hello Parallel World,6,7,8,9,10" << std::endl;
	}

	{ // File Input Stream - Used to read data from a file
		std::ifstream inputStream("TestData.csv");

		// Loop while the file stream is valid
		while (inputStream.good())
		{
			// Attempt to read an entire line
			std::string csvLine;
			std::getline(inputStream, csvLine);

			// If the line was empty then break out of the loop
			if (csvLine.length() == 0)
				break;

			// Setup a string stream with the line that we read in as the source
			std::istringstream lineStream(csvLine);

			// Read in the message from the stream
			std::string message;
			std::getline(lineStream, message, ',');
			std::cout << "Read in " << message << std::endl;

			// Continue reading from the stream as long as the data is good
			while (lineStream.good())
			{
				int intValue;
				lineStream >> intValue;

				// Read in the dummy character if the stream still has data
				if (lineStream.good())
				{
					char dummyChar;
					lineStream >> dummyChar;
				}

				std::cout << "Read in " << intValue << std::endl;
			}
		}
	}

	return 0;
}