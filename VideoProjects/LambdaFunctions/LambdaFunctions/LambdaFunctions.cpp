#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> intVector;
	intVector.push_back(3);
	intVector.push_back(8);
	intVector.push_back(2);
	intVector.push_back(1);
	intVector.push_back(38);

	// Setup a prefix for the numbers
	std::string prefix = "  : ";
	int sumOfValues = 0;

	// Store the lambda in a variable
	auto lambdaFn = [prefix, &sumOfValues](const int& intValue) {
		std::cout << prefix << intValue << std::endl;

		sumOfValues += intValue;
	};

	// Iterating over the intVector and calling lambdaFn on every element
	std::for_each(intVector.begin(), intVector.end(), lambdaFn);

	return 0;
}

