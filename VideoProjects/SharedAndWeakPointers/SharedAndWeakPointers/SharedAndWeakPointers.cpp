#include "stdafx.h"
#include "SharedPtr.h"
#include "WeakPtr.h"

#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	{ // Define a new scope for testing the shared pointer and weak pointer
		SharedPtr ptr1(new int[5], true);

		{ // Test the increasing/decreasing of the reference count
			SharedPtr ptr2 = ptr1;

			std::cout << "Reference count should be 2" << std::endl;
		}

		std::cout << "Reference count should be 1" << std::endl;

		{ // Test basic weak pointer behaviour
			WeakPtr ptr2 = ptr1;

			{
				WeakPtr ptr3 = ptr2;

				std::cout << "Reference count should be 1" << std::endl;
			}

			std::cout << "Reference count should be 1" << std::endl;
		}

		std::cout << "Reference count should be 1" << std::endl;

		{ // Test converting a weak pointer to a shared pointer
			WeakPtr ptr2 = ptr1;

			{
				SharedPtr ptr3 = ptr2.GetSharedPtr();

				std::cout << "Reference count should be 2" << std::endl;
			}

			std::cout << "Reference count should be 1" << std::endl;
		}

		std::cout << "Reference count should be 1" << std::endl;

		{ // Test assignment of a shared pointer
			SharedPtr ptr2;
			ptr2 = ptr1;

			std::cout << "Reference count should be 2" << std::endl;
		}

		std::cout << "Reference count should be 1" << std::endl;
	}

	std::cout << "Data has been released" << std::endl;

	return 0;
}

