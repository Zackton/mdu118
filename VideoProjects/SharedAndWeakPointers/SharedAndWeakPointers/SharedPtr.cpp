#include "stdafx.h"
#include "SharedPtr.h"
#include "WeakPtr.h"
#include <algorithm>

SharedPtr::SharedPtr()
{
	dataPtr = nullptr;
	referenceCountPtr = nullptr;
}

SharedPtr::SharedPtr(void* _dataPtr, bool _isArray)
{
	dataPtr = _dataPtr;
	isArray = _isArray;
	referenceCountPtr = nullptr;

	IncreaseRefCount();
}

SharedPtr::SharedPtr(const SharedPtr& other)
{
	dataPtr = other.dataPtr;
	isArray = other.isArray;
	referenceCountPtr = other.referenceCountPtr;
	linkedWeakPointers = other.linkedWeakPointers;

	IncreaseRefCount();
}

SharedPtr::~SharedPtr()
{
	// Only decreasing the reference count if the shared pointer currently
	// refers to valid data.
	if (IsValid())
	{
		DecreaseRefCount();
	}
}

bool SharedPtr::IsValid() const
{
	return dataPtr && referenceCountPtr;
}

SharedPtr& SharedPtr::operator = (const SharedPtr& rhs)
{
	// Check if we are currently a valid shared point
	if (IsValid())
	{
		// If we are assigning ourself to ourself then we do nothing
		if ((dataPtr == rhs.dataPtr) && (referenceCountPtr == rhs.referenceCountPtr))
		{
			return *this;
		}

		// We are no longer referring to the same data so decrease the reference count
		DecreaseRefCount();
	}
	
	// Transfer the pointer information
	dataPtr = rhs.dataPtr;
	isArray = rhs.isArray;
	referenceCountPtr = rhs.referenceCountPtr;
	linkedWeakPointers = rhs.linkedWeakPointers;

	// Increase the reference count but only if we are pointing to valid data
	if (IsValid())
		IncreaseRefCount();

	return *this;
}

void SharedPtr::IncreaseRefCount()
{
	// If we haven't yet setup the reference count then allocate it
	if (!referenceCountPtr)
	{
		referenceCountPtr = new size_t;
		*referenceCountPtr = 0;
	}

	// Increment the reference count pointer
	++(*referenceCountPtr);
}

void SharedPtr::DecreaseRefCount()
{
	// Decrement the reference count pointer
	--(*referenceCountPtr);

	// Check if no one else is referring to the data
	if (*referenceCountPtr == 0)
	{
		// Invalidate all of the weak pointers that are linked
		InvalidateLinkedWeakPointers();

		// Delete the reference count pointer
		delete referenceCountPtr;
		referenceCountPtr = nullptr;

		// Delete the dataPtr using the appropriate method
		if (isArray)
			delete[] dataPtr;
		else
			delete dataPtr;
		dataPtr = nullptr;
	}
}

void SharedPtr::AddLinkedWeakPointer(const WeakPtr& weakPtrToLink) const
{
	linkedWeakPointers.push_back(&weakPtrToLink);
}

void SharedPtr::RemoveLinkedWeakPointer(const WeakPtr& weakPtrToUnlink) const
{
	// Reorder the list so that the elements to remove are at the back of the list
	auto newEndPoint = std::remove_if(linkedWeakPointers.begin(), linkedWeakPointers.end(), [&weakPtrToUnlink](const WeakPtr* weakPtrToTest) {
		return weakPtrToTest == &weakPtrToUnlink;
	});

	// Remove the elements that are no longer required (ie. weakPtrToUnlink)
	linkedWeakPointers.erase(newEndPoint, linkedWeakPointers.end());
}

void SharedPtr::InvalidateLinkedWeakPointers()
{
	// Invalidate all of the linked weak pointers
	for (auto weakPtr : linkedWeakPointers)
	{
		weakPtr->Invalidate();
	}
}
