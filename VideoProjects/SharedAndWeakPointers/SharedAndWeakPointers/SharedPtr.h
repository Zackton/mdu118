#pragma once

#include <list>

// Forward declare WeakPtr so we have access to it
class WeakPtr;

class SharedPtr
{
public:
	SharedPtr();
	SharedPtr(void* _dataPtr, bool _isArray);
	SharedPtr(const SharedPtr& other);

	~SharedPtr();

	bool IsValid() const;

	SharedPtr& operator = (const SharedPtr& rhs);

	void* GetData() { return dataPtr; }

private:
	void IncreaseRefCount();
	void DecreaseRefCount();

	void AddLinkedWeakPointer(const WeakPtr& weakPtrToLink) const;
	void RemoveLinkedWeakPointer(const WeakPtr& weakPtrToUnlink) const;
	void InvalidateLinkedWeakPointers();

private:
	void* dataPtr;
	bool isArray;
	size_t* referenceCountPtr;

	mutable std::list<const WeakPtr*> linkedWeakPointers;

	friend class WeakPtr;
};

