#include "stdafx.h"
#include "WeakPtr.h"
#include "SharedPtr.h"

WeakPtr::WeakPtr()
{
	linkedSharedPtr = nullptr;
}

WeakPtr::WeakPtr(const SharedPtr& other)
{
	linkedSharedPtr = &other;

	// If we have valid data then inform the shared pointer of the link
	if (IsValid())
	{
		linkedSharedPtr->AddLinkedWeakPointer(*this);
	}
}

WeakPtr::WeakPtr(const WeakPtr& other)
{
	linkedSharedPtr = other.linkedSharedPtr;

	// If we have valid data then inform the shared pointer of the link
	if (IsValid())
	{
		linkedSharedPtr->AddLinkedWeakPointer(*this);
	}
}

WeakPtr::~WeakPtr()
{
	// Remove the link to the weak pointer on destruction
	if (IsValid())
	{
		linkedSharedPtr->RemoveLinkedWeakPointer(*this);
		linkedSharedPtr = nullptr;
	}
}

WeakPtr& WeakPtr::operator = (const SharedPtr& rhs)
{
	// If we are already linked to a shared pointer then remove the link
	if (IsValid())
	{
		linkedSharedPtr->RemoveLinkedWeakPointer(*this);
	}

	linkedSharedPtr = &rhs;

	// If we have valid data then inform the shared pointer of the link
	if (IsValid())
	{
		linkedSharedPtr->AddLinkedWeakPointer(*this);
	}

	return *this;
}

WeakPtr& WeakPtr::operator = (const WeakPtr& rhs)
{
	// If we are already linked to a shared pointer then remove the link
	if (IsValid())
	{
		linkedSharedPtr->RemoveLinkedWeakPointer(*this);
	}

	linkedSharedPtr = rhs.linkedSharedPtr;

	// If we have valid data then inform the shared pointer of the link
	if (IsValid())
	{
		linkedSharedPtr->AddLinkedWeakPointer(*this);
	}

	return *this;
}

SharedPtr WeakPtr::GetSharedPtr()
{
	return *linkedSharedPtr;
}

bool WeakPtr::IsValid() const
{
	return linkedSharedPtr && linkedSharedPtr->IsValid();
}

void WeakPtr::Invalidate() const
{
	linkedSharedPtr = nullptr;
}
