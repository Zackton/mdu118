#pragma once

// Forward declare the SharedPtr class
class SharedPtr;

class WeakPtr
{
public:
	WeakPtr();
	WeakPtr(const SharedPtr& other);
	WeakPtr(const WeakPtr& other);
	~WeakPtr();

	WeakPtr& operator = (const SharedPtr& rhs);
	WeakPtr& operator = (const WeakPtr& rhs);

	SharedPtr GetSharedPtr();
	bool IsValid() const;
	void Invalidate() const;

private:
	mutable const SharedPtr* linkedSharedPtr;
};

