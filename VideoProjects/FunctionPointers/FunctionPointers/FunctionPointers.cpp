#include "stdafx.h"
#include <iostream>

// Set up a couple of basic functions
int Square(int inputValue)
{
	return inputValue * inputValue;
}

int Cube(int inputValue)
{
	return inputValue * inputValue * inputValue;
}

// Define a new type for our function pointers to the Square and Cube functions
typedef int(*IntegerPowerFn)(int);

// Setup a simple class to demonstrate pointer to member function
class SimpleClass
{
public:
	int Multiply(int lhs, int rhs)
	{
		return lhs * rhs;
	}
};

// Define a new type for our pointer to the Multiply method
typedef int(SimpleClass::*MathFn)(int, int);

int _tmain(int argc, _TCHAR* argv[])
{
	{ // Example of function pointers
		// Bind the squareFn variable to the Square function
		IntegerPowerFn squareFn = Square;

		// Bind the cubeFn variable to the Cube function
		IntegerPowerFn cubeFn = Cube;

		std::cout << "Value is 2" << std::endl;
		std::cout << "   - Value^2 = " << squareFn(2) << std::endl;
		std::cout << "   - Value^3 = " << cubeFn(2) << std::endl;
	}

	{ // Example of pointer to member function
		// Bind the multiplyFn variable to the Multiply function
		MathFn multiplyFn = &SimpleClass::Multiply;
		SimpleClass simpleClass;

		std::cout << "3 x 4 = " << (simpleClass.*(multiplyFn))(3, 4) << std::endl;
	}

	return 0;
}

