#include "stdafx.h"
#include <string>
#include <iostream>

class ExampleClass
{
public:
	ExampleClass(const std::string& _name)
	{
		name = _name;
	}

	// The equality operator has been marked as const making it able to be used
	// universally regardless of if the data being compared is const or non-const
	bool operator == (const ExampleClass& rhs) const
	{
		return name == rhs.name;
	}

	void ExampleFunction() const
	{
		// Cast the pointer to ourselves (this) to a non-const version of the pointer so we can modify it
		ExampleClass* nonConstThis = const_cast<ExampleClass*>(this);

		// Change the name on the non-const version of ourselves
		nonConstThis->name = "test";
	}

private:
	std::string name;
};

int _tmain(int argc, _TCHAR* argv[])
{
	const ExampleClass constExample("ConstExample");
	ExampleClass example1("Example1");

	// Run the equality operator with the const object on the left hand side
	if (constExample == example1)
	{
		std::cout << "Example classes match!" << std::endl;
	}

	// Run the equality operator with the const object on the right hand side
	if (example1 == constExample)
	{
		std::cout << "Example classes match!" << std::endl;
	}

	return 0;
}

