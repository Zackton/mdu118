#include "stdafx.h"
#include <iostream>

// This will create a copy of the value that has been dynamically allocated. 
// This is generally NOT what we want.
void TestFunction_Value(int intValue)
{
	std::cout << "Value: intValue is at " << std::hex << &intValue << std::dec << " and it's value is " << intValue << std::endl;
}

// This will NOT create a copy of the value that has been dynamically allocated.
// In general if you derefence a pointer it should only be to then assign it to a reference.
void TestFunction_Reference(int& intValue)
{
	std::cout << "Reference: intValue is at " << std::hex << &intValue << std::dec << " and it's value is " << intValue << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	{ // Simple allocation of a single value
		int* intValue = new int(42);

		std::cout << "The location in memory for intValue is " << std::hex << intValue << std::dec << std::endl;
		std::cout << "The value of intValue is " << *intValue << std::endl;

		TestFunction_Value(*intValue);
		TestFunction_Reference(*intValue);

		// Free the allocated data
		delete intValue;

		// Set the pointer to nullptr. This flags the pointer explicitly as being invalid
		intValue = nullptr;
	}

	{ // Allocation of an array
		const int NumElements = 5;
		int* intArray = new int[NumElements];

		// Populate the array
		for (int index = 0; index < NumElements; ++index)
		{
			intArray[index] = index;
			std::cout << "Setting element " << index << " to " << index << std::endl;
		}

		// Free the allocated data and inform delete that it is cleaning up an array not a single value
		delete[] intArray;

		// Set the pointer to nullptr. This flags the pointer explicitly as being invalid
		intArray = nullptr;
	}

	return 0;
}

