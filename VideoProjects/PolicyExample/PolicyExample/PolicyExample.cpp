#include "stdafx.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>
#include <locale>

// Setup a class that can convert a string to upper case
class Uppercase
{
public:
	static std::string Execute(const std::string& input)
	{
		std::string output;

		for (auto character : input)
		{
			output += std::toupper(character);
		}

		return output;
	}
};

// Setup a class that can convert a string to lower case
class Lowercase
{
public:
	static std::string Execute(const std::string& input)
	{
		std::string output;

		for (auto character : input)
		{
			output += std::tolower(character);
		}

		return output;
	}
};

// Setup a class that can flip the case of characters in a string
class FlippedCase
{
public:
	static std::string Execute(const std::string& input)
	{
		std::string output;

		for (auto character : input)
		{
			if (std::islower(character))
				output += std::toupper(character);
			else
				output += std::tolower(character);
		}

		return output;
	}
};

// Setup a templated class that can switch between different string manipulation methods
// based upon the parameter provided
template <typename ManipulationType>
class ManipulateString
{
public:
	static std::string Perform(const std::string& input)
	{
		return ManipulationType::Execute(input);
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	{	
		const std::string testString = "This is a test string!";

		std::cout << "Uppercase: " << ManipulateString<Uppercase>::Perform(testString) << std::endl;
		std::cout << "Lowercase: " << ManipulateString<Lowercase>::Perform(testString) << std::endl;
		std::cout << "Flipped Case: " << ManipulateString<FlippedCase>::Perform(testString) << std::endl;
	}

	return 0;
}

