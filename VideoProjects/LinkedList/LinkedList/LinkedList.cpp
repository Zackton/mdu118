#include "stdafx.h"
#include "List.h"
#include <iostream>

int _tmain(int argc, _TCHAR* argv[])
{
	List<int> integerList;

	integerList.PushBack(1);
	integerList.PushBack(2);
	integerList.PushBack(3);
	integerList.PushBack(4);
	integerList.PushBack(5);
	integerList.PushFront(0);

	ListElement<int>* current = integerList.GetFirst();
	while (current)
	{
		std::cout << current->GetValue() << std::endl;

		current = current->GetNext();
	}

	return 0;
}

