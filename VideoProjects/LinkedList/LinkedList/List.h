#ifndef __LIST_H
#define __LIST_H

#include <assert.h>
#include <iostream>

template <typename ElementType>
class List;

template <typename ElementType>
class ListElement
{
public:
	ListElement(const ElementType& _value) :
		value(_value),
		previous(nullptr),
		next(nullptr)
	{
	}

	ListElement<ElementType>* GetPrevious()
	{
		return previous;
	}

	ListElement<ElementType>* GetNext()
	{
		return next;
	}

	ElementType& GetValue()
	{
		return value;
	}

private:
	ElementType value;

	ListElement<ElementType>* previous;	// pointer to the previous element
	ListElement<ElementType>* next;		// pointer to the next element

	friend List < ElementType > ;
};

template <typename ElementType>
class List
{
public:
	List() :
		head(nullptr),
		tail(nullptr)
	{
	}

	~List();

	void PushFront(const ElementType& value);
	void PushBack(const ElementType& value);

	ElementType PopFront();
	ElementType PopBack();

	ListElement<ElementType>* GetFirst() 
	{
		return head; 
	}

	ListElement<ElementType>* GetLast()
	{
		return tail;
	}

	bool HasData() const
	{
		return head != nullptr;
	}

private:
	ListElement<ElementType>* head; // pointer to the first element in the list
	ListElement<ElementType>* tail; // pointer to the last element in the list
};

template <typename ElementType>
List<ElementType>::~List()
{
	ListElement<ElementType>* currentElement = head;

	while (currentElement)
	{
		// Saving the pointer of the element to delete
		ListElement<ElementType>* elementToDelete = currentElement;

		// Move to the next element
		currentElement = currentElement->next;

		// Delete the element
		delete elementToDelete;
	}
}

template <typename ElementType>
void List<ElementType>::PushFront(const ElementType& value)
{
	// Create new element
	ListElement<ElementType>* newElement = new ListElement<ElementType>(value);

	// is the list currently empty?
	if (head == nullptr)
	{
		assert(tail == nullptr);

		head = tail = newElement;

		return;
	}

	assert(head != nullptr);
	assert(tail != nullptr);

	// Setup the links between the elements
	newElement->next = head;
	head->previous = newElement;

	// Update the head element
	head = newElement;
}

template <typename ElementType>
void List<ElementType>::PushBack(const ElementType& value)
{
	// Create new element
	ListElement<ElementType>* newElement = new ListElement<ElementType>(value);

	// is the list currently empty?
	if (head == nullptr)
	{
		assert(tail == nullptr);

		head = tail = newElement;

		return;
	}

	assert(head != nullptr);
	assert(tail != nullptr);

	// Setup the links between the elements
	tail->next = newElement;
	newElement->previous = tail;

	// Update the tail elements
	tail = newElement;
}

template <typename ElementType>
ElementType List<ElementType>::PopFront()
{
	assert(head != nullptr);
	assert(tail != nullptr);

	// Retrieve the current head and it's value
	ListElement<ElementType>* currentHead = head;
	ElementType valueToReturn = currentHead->value;

	// Move the head element to the next element
	head = head->next;
	if (head)
		head->previous = nullptr;
	else
		tail = nullptr;

	// Free the current head element
	delete currentHead;

	return valueToReturn;
}

template <typename ElementType>
ElementType List<ElementType>::PopBack()
{
	assert(head != nullptr);
	assert(tail != nullptr);

	// Retrieve the current tail and it's value
	ListElement<ElementType>* currentTail = tail;
	ElementType valueToReturn = currentTail->value;

	// Move the tail element to the previous element
	tail = tail->previous;
	if (tail)
		tail->next = nullptr;
	else
		head = nullptr;

	// Free the current tail element
	delete currentTail;

	return valueToReturn;
}

#endif // __LIST_H