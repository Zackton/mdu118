#include "stdafx.h"
#include <iostream>

void FnPassByValue(int intValue)
{
	intValue = 42;
}

void FnPassByPointer(int* intPtr)
{
	*intPtr = 42;
}

void FnPassByReference(int& intRef)
{
	intRef = 42;
}

void FnPassByConstReference(const int& constIntRef)
{
}

int ReturnByValue()
{
	int intValue = 42;

	return intValue;
}

int globalInteger = 42;

int* ReturnByPointer()
{
	return &globalInteger;
}

int& ReturnByReference()
{
	return globalInteger;
}

const int& ReturnByConstReference()
{
	return globalInteger;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int testInteger = 0;

	std::cout << "Pass By Value Test" << std::endl;
	testInteger = 0;
	std::cout << "testInteger is " << testInteger << std::endl;
	FnPassByValue(testInteger);
	std::cout << "testInteger is now " << testInteger << std::endl;

	std::cout << std::endl << "Pass By Pointer Test" << std::endl;
	testInteger = 0;
	std::cout << "testInteger is " << testInteger << std::endl;
	FnPassByPointer(&testInteger);
	std::cout << "testInteger is now " << testInteger << std::endl;

	std::cout << std::endl << "Pass By Reference Test" << std::endl;
	testInteger = 0;
	std::cout << "testInteger is " << testInteger << std::endl;
	FnPassByReference(testInteger);
	std::cout << "testInteger is now " << testInteger << std::endl;

	return 0;
}

