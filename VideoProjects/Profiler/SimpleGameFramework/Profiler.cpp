#include "stdafx.h"
#include "Profiler.h"
#include <sstream>

ProfileSample::ProfileSample(const std::string& _name) :
	name(_name)
{
	startTime = CPUUtils::GetPerformanceCounterTicks();
}

ProfileSample::~ProfileSample()
{
	LONGLONG endTime = CPUUtils::GetPerformanceCounterTicks();

	// Calculate the delta time
	LONGLONG deltaTimeInTicks = endTime - startTime;

	// Record the profile information
	ProfilerInstance.RecordSample(name, deltaTimeInTicks);
}

Profiler::Profiler()
{
}

Profiler::~Profiler()
{
}

void Profiler::RecordSample(const std::string& name, LONGLONG deltaTimeInTicks)
{
	ProfileDataMapIter foundProfileIt = recordedProfiles.find(name);

	// Does the sample already exist?
	if (foundProfileIt != recordedProfiles.end())
	{
		ProfileData& currentData = foundProfileIt->second;

		// Update the profile data
		++currentData.callCount;
		currentData.totalTimeInTicks += deltaTimeInTicks;
		currentData.minTimeInTicks = min(currentData.minTimeInTicks, deltaTimeInTicks);
		currentData.maxTimeInTicks = max(currentData.maxTimeInTicks, deltaTimeInTicks);
	}
	else
	{
		ProfileData newData;

		// Setup the new profile data
		newData.callCount = 1;
		newData.totalTimeInTicks = newData.minTimeInTicks = newData.maxTimeInTicks = deltaTimeInTicks;

		recordedProfiles[name] = newData;
	}
}

std::string Profiler::GetProfileInfo()
{
	LONGLONG ticksPerSecond = CPUUtils::GetPerformanceCounterFrequency();
	std::ostringstream profileStream;

	// Iterate over the profile data
	for (auto profilePair : recordedProfiles)
	{
		const std::string& name = profilePair.first;
		const ProfileData& data = profilePair.second;

		// Calculate the times in seconds
		const double totalTime = (double)data.totalTimeInTicks / ticksPerSecond;
		const double minTime = (double)data.minTimeInTicks / ticksPerSecond;
		const double maxTime = (double)data.maxTimeInTicks / ticksPerSecond;
		const double averageTime = totalTime / data.callCount;

		profileStream << name << " : "
					  << "called " << data.callCount << " times" << std::endl
					  << "  Average Time: " << averageTime * 1000 << " ms" << std::endl
					  << "  Minimum Time: " << minTime * 1000 << " ms" << std::endl
					  << "  Maximum Time: " << maxTime * 1000 << " ms" << std::endl;
	}

	return profileStream.str();
}
