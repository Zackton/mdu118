#pragma once

#include "CPUUtils.h"
#include <string>
#include <map>

class ProfileSample
{
public:
	ProfileSample(const std::string& _name);
	~ProfileSample();

private:
	std::string name;
	LONGLONG startTime;
};

struct ProfileData
{
	LONGLONG callCount;
	LONGLONG totalTimeInTicks;
	LONGLONG minTimeInTicks;
	LONGLONG maxTimeInTicks;
};

class Profiler
{
public:
	static Profiler& Instance()
	{
		static Profiler instance;

		return instance;
	}

	~Profiler();

	void RecordSample(const std::string& name, LONGLONG deltaTimeInTicks);

	std::string GetProfileInfo();

private:
	Profiler();

	typedef std::map<std::string, ProfileData> ProfileDataMap;
	typedef ProfileDataMap::iterator ProfileDataMapIter;
	ProfileDataMap recordedProfiles;
};

#define ProfilerInstance (Profiler::Instance())

#define ConcateInternal(val1, val2) val1##val2
#define Concate(val1, val2) ConcateInternal(val1, val2)

#define BeginProfile ProfileSample Concate(__profile_,__LINE__)(__FUNCTION__);
#define BeginNamedProfile(name) ProfileSample Concate(__profile_,__LINE__)(name);
