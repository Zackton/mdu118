#include "EntityFactory.h"
#include "LootEntity.h"
#include "WeaponEntity.h"

//////////////////////////////////////////////////////////////////////////
// Private Implementation for Entity Factory

class FactoryImpl : public ObserverBase
{
public:
	virtual void Destroyed(ObservableBase object);

	EntityBase* CreateEntity(EntityType type);

protected:
	std::set<EntityBase*> ownedEntities;
};

EntityBase* FactoryImpl::CreateEntity(EntityType type)
{
	EntityBase* newEntity = nullptr;

	// Based on the requested entity type instantiate the correct object
	if (type == eetLoot)
	{
		newEntity = new LootEntity();
	}
	else if (type == eetWeapon)
	{
		newEntity = new LootEntity();
	}

	// If we succesfully created an entity then add ourselves as the observer
	// and add it to our list of owned entities
	if (newEntity)
	{
		newEntity->AddObserver(*this);
		ownedEntities.insert(newEntity);
	}

	return nullptr;
}

void FactoryImpl::Destroyed(ObservableBase& object)
{
	ownedEntities.erase(dynamic_cast<EntityBase*>(&object));
}

//////////////////////////////////////////////////////////////////////////
// Public Implementation for Entity Factory

EntityFactory::EntityFactory()
{
	factoryImpl = new FactoryImpl();
}

EntityFactory::~EntityFactory()
{
	delete factoryImpl;
}

EntityBase* EntityFactory::CreateEntity(EntityType type)
{
	return factoryImpl->CreateEntity(type);
}
