#pragma once

#include "EntityBase.h"

class FactoryImpl;

class LootEntity :
	public EntityBase
{
public:
	virtual ~LootEntity();

	virtual std::string IdentifyMe() const { return "LootEntity"; }

	// We want to enforce that all entity creation happens through our factory.
	// To do this we make the constructor protected.
protected:
	LootEntity();
};

