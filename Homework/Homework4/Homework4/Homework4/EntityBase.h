#pragma once

#include <string>

#include "ObservableBase.h"

class FactoryImpl;

class EntityBase : public ObservableBase
{
public:
	virtual ~EntityBase();

	virtual std::string IdentifyMe() const { return "EntityBase"; }

	// We want to enforce that all entity creation happens through our factory.
	// To do this we make the constructor protected.
protected:
	EntityBase();
};

