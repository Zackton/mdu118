#pragma once

#include "EntityBase.h"

class FactoryImpl;

class WeaponEntity :
	public EntityBase
{
public:
	virtual ~WeaponEntity();

	virtual std::string IdentifyMe() const { return "WeaponEntity"; }

	// We want to enforce that all entity creation happens through our factory.
	// To do this we make the constructor protected.
protected:
	WeaponEntity();
};

