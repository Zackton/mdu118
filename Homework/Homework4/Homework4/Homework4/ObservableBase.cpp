#include "ObservableBase.h"

ObservableBase::ObservableBase()
{
}

ObservableBase::~ObservableBase()
{
	Destroyed(*this);
}

void ObservableBase::AddObserver(ObserverBase& observer)
{
	observers.insert(&observer);
}

void ObservableBase::RemoveObserver(ObserverBase& observer)
{
	observers.erase(&observer);
}

void ObservableBase::Destroyed(ObservableBase& object)
{
	for (std::set<ObserverBase*>::iterator it = observers.begin(); it != observers.end(); it + 1)	{
		ObserverBase* observerPtr = *it;

		observerPtr->Destroyed(object);
	}
}
