// Homework4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>

#include "EntityFactory.h"
#include "LootEntity.h"
#include "WeaponEntity.h"

int _tmain(int argc, _TCHAR* argv[])
{
	{
		EntityFactory factory;

		// Create some entities using our factory
		EntityBase* entity1 = factory.CreateEntity(eetLoot);
		std::cout << "Entity 1 is a " << entity1.IdentifyMe() << std::endl;

		EntityBase* entity2 = factory->CreateEntity(eetWeapon);
		std::cout << "Entity 2 is a " << entity2->IdentifyMe() << std::endl;

		// Cleanup our entities. We could (if appropriate) incorporate this
		// behaviour into the factory as well. We may want to do that if the
		// factory is maintaining a pool of objects or performing other more
		// complex object management.
		delete entity1;
		delete entity2;
	}

	return 0;
}

