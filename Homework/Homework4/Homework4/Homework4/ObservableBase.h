#pragma once

#include <set>

class ObservableBase;

class ObserverBase
{
public:
	// All observers must implement the Destroyed method
	virtual void Destroyed(ObservableBase& object) = 0;
};

class ObservableBase
{
public:
	ObservableBase();
	virtual ~ObservableBase();

	// These helper functions manage the list of observers for the object
	void AddObserver(ObserverBase& observer);
	void RemoveObserver(ObserverBase& observer);

private:
	void Destroyed(ObservableBase& object);

private:
	std::set<ObserverBase*> observers;
};

