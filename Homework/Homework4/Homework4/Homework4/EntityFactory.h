#pragma once

#include "ObservableBase.h"

// Define the different types of entities that we can create
typedef enum {
	eetWeapon,
	eetLoot,

	eetNumEntityTypes
} EntityType;

// Forward declare our classes so we can use them in the factory
class EntityBase;
class FactoryImpl;

class EntityFactory
{
public:
	EntityFactory();
	virtual ~EntityFactory();

	EntityBase* CreateEntity(EntityType type);

	// We want to fully isolate the internal factory logic so we use the PImpl
	// approach and wrap the behaviour in a separate class.
private:
	FactoryImpl* factoryImpl;
};

