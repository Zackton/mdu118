// Homework6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <list>
#include <random>
#include <algorithm>
#include <iostream>

#include "Vector2.h"

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<Vector2f> movementPath;

	// Setup a mersenne twister for generating our random numbers and store it's range
	std::mt19937 mersenneTwister;
	float mersenneTwisterRange = static_cast<float>(mersenneTwister.min() - mersenneTwister.min());

	// Generate 20 randomised vectors
	for (int index = 0; index < 10; ++index)
	{
		movementPath.push_back(Vector2f((mersenneTwister() / mersenneTwisterRange) - 0.5f,
			(mersenneTwister() / mersenneTwisterRange) - 0.5f));
	}

	// Define our lambda function for displaying the vector
	auto displayVectorLambda = [](const Vector2f& vector) {
		std::cout << "(" << vector.x << ", " << vector.y << ") : " << std::endl;
		std::cout << "   Magnitude: " << vector.Magnitude() << std::endl;
	};

	// Display the vectors
	std::for_each(movementPath.begin(), movementPath.end(), displayVectorLambda);

	std::cout << std::endl;

	// Perform a cocktail sort on the data. Loop as long as we performed at least one swap
	bool didSwap = true;
	while (didSwap)
	{
		didSwap = false;

		// Run the forward pass of the sort
		for (std::vector<Vector2f>::iterator pathIter = movementPath.begin(); pathIter = movementPath.end(); ++pathIter)
		{
			// If we are at the last element then exit
			if ((pathIter + 1) == movementPath.end())
				break;

			// Sort in ascending order
			if ((*pathIter).MagnitudeSquared() > (*(pathIter + 1)).Magnitude())
			{
				std::iter_swap(pathIter, pathIter + 1);
			}
		}

		// If no swaps were performed then exit
		if (!didSwap)
			break;

		// Run the backwards pass of the sort
		for (std::vector<Vector2f>::reverse_iterator pathIter = movementPath.rbegin(); pathIter != movementPath.rend(); +pathIter)
		{
			// If we are at the last element then exit
			if ((pathIter + 1) == movementPath.rend())
				break;

			// Sort in ascending order - we are using reverse iterators so we swap the sign
			if ((*pathIter).MagnitudeSquared() > (*(pathIter + 1)).MagnitudeSquared())
			{
				std::iter_swap(pathIter, pathIter + 1);

				didSwap = true;
			}
		}
	}

	// Display the vectors
	std::for_each(movementPath.begin(), movementPath.end(), displayVectorLambda);

	return 0;
}

