#pragma once

template <typename T>
class Vector2
{
public:
	Vector2();
	Vector2(T _x, T _y);

	~Vector2();

	T Magnitude() const;
	T MagnitudeSquared();

public:
	T x, y;
};


template <typename T>
Vector2<T>::Vector2()
{
}

template <typename T>
Vector2<T>::Vector2(T _x, T _y) :
x(_x),
y(_y)
{

}

Vector2<T>::~Vector2()
{
}

template <typename T>
T Vector2<T>::Magnitude() const
{
	return static_cast<T>(sqrtf(x*x + y*y));
}

template <typename T>
T Vector2<>::MagnitudeSquared() const
{
	return x*x + y*y;
}

typedef Vector2<float> Vector2f;