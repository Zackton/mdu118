// Homework2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

struct Vector3f
{
	float x;
	float y;
	float z;
};

// Setup our base class for all vehicles
class VehicleBase
{
public:
	VehicleBase();
	~VehicleBase();

	std::string ClassId() { return "VehicleBase"; }
	void PrintId();

	virtual void Placeholder() {}

protected:
	Vector3f* path;
};

VehicleBase::VehicleBase()
{
	path = new Vector3f[10];
}

VehicleBase::~VehicleBase()
{
	delete[] path;

	std::cout << "Freeing path" << std::endl;
}

void VehicleBase::PrintId()
{
	std::cout << "Object is a " << this->ClassId() << std::endl;
}

// All aircraft will belong to the Aircraft class
class Aircraft : protected VehicleBase
{
public:
	~Aircraft();

	std::string ClassId() { return "Aircraft"; }

	virtual void Placeholder() {}
};

Aircraft::~Aircraft()
{
}

// Setup our class for the F22 aircraft
class F22Raptor : protected Aircraft
{
public:
	F22Raptor();
	~F22Raptor();

	std::string ClassId() { return "F22Raptor"; }

	virtual void Placeholder() {}

protected:
	Vector3f* targets;
};

F22Raptor::F22Raptor()
{
	targets = new Vector3f[10];
}

F22Raptor::~F22Raptor()
{
	delete[] targets;

	std::cout << "Freeing targets" << std::endl;
}

void saveVehicle(VehicleBase vehicle)
{
	std::cout << "Saving vehicle " << vehicle.ClassId() << std::endl;
}

// The output you should see is:
//raptorObj is a F22Raptor
//Object is a F22Raptor
//Freeing targets
//Freeing path
//raptorVal is a F22Raptor
//Saving vehicle F22Raptor
//Freeing targets
//Freeing path

int _tmain(int argc, _TCHAR* argv[])
{
	{
		VehicleBase* vehicleObj = new F22Raptor();

		// The output from these two lines should match
		std::cout << "raptorObj is a " << vehicleObj->ClassId() << std::endl;
		vehicleObj->PrintId();

		// We're done with the vehicleObj so free it up
		delete vehicleObj;
	}

	{
		F22Raptor raptorVal;

		std::cout << "raptorVal is a " << raptorVal.ClassId() << std::endl;

		// We're done with raptorVal so free it
		delete &raptorVal;

		// Save the vehicle
		saveVehicle(raptorVal);
	}

	return 0;
}
