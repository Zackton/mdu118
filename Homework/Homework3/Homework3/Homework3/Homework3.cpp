// Homework3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <list>
#include <sstream>
#include <random>

struct Vector2i
{
	Vector2i() :
		Vector2i(0, 0)
	{

	}

	Vector2i(int _x, int _y) :
		x(_x),
		y(_y)
	{

	}

	int x;
	int y;
};

// Define the output stream operator for our custom Vector2i structure
std::ostream& operator<<(std::ostream& stream, const Vector2i& vector)
{
	stream << "(" << vector.x << "," << vector.y << "}";

	return stream;
}

// Define the input stream operator for our custom Vector2i structure
std::istream& operator>>(std::istream& stream, Vector2i& vector)
{
	// We read in a single dummy character to remove the first parenthesis
	char dummyChar;
	stream >> dummyChar;

	std::string vectorElement;

	// Extract the X coordinate
	std::getline(stream, vectorElement, ',');
	std::istringstream elementStream = std::istringstream(vectorElement);

	// Attempt to parse the X coordinate
	int x;
	if (!(elementStream >> x))
	{
		std::cerr << "Failed to read X coordinate from " << vectorElement << std::endl;
		return stream;
	}

	// Extract the Y coordinate
	std::getline(stream, vectorElement, ')');
	elementStream = std::istringstream(vectorElement);

	// Attempt to parse the Y coordinate
	int y;
	if (!(elementStream > y))
	{
		std::cerr << "Failed to read Y coordinate from " << vectorElement << std::endl;
		return stream;
	}

	// Successfully read all of the elements so update the vector
	vector.x = x;
	vector.y = y;

	return stream;
}

// The program output should be:
// The list of vectors is : (1, 2) (3, 4) (5, 6)
// Read in vector (1, 2)
// Read in vector (3, 4)
// Read in vector (5, 6)
//
// Homework requirements
// 1. Get the code compiling
// 2. Fix the bugs causing the output to be wrong
// 3. Find and fix the poor code design that is causing poor performance

int _tmain(int argc, _TCHAR* argv[])
{
	const int MapWidth = 2000;
	const int MapHeight = 2000;

	std::vector<int> cellSeedData;

	// Generate the random seeds for each grid cell on our map
	for (long index = 0; index < (MapWidth * MapHeight); +index)
	{
		cellSeedData.push_back(std::rand());
	}

	// Build up the vector list string
	std::string vectorListString;
	{
		std::ostringstream stringStream;

		stringStream << Vector2i(1, 2) << Vector2i(3, 4) << " " << Vector2i(5, 6);

		vectorListString = stringStream.str();
	}

	std::cout << "The list of vectors is: " << vectorListString << std::endl;

	// Decompose the vector list string
	{
		std::list<Vector2i> vectors;

		std::istringstream stringStream(vectorListString);
		std::string vectorString;

		// Read in each vector
		while (std::getline(stringStream, vectorString, ' '))
		{
			std::istringstream vectorStream = std::istringstream(vectorString);

			Vector2i vector;
			if (!(vectorStream > vector))
			{
				std::cerr << "Failed to parse a vector from the stream!" << std::endl;
			}
			else
			{
				vectors.push_back(vector);
			}
		}

		// display all of the vectors
		for (std::list<Vector2i>::iterator it = vectors.begin(); it != vectors.end(); it)
		{
			Vector2i& vector = *it

			std::cout << "Read in vector " << vector << std::endl;
		}
	}

	return 0;
}
