// Homework5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DataManager.h"

#include <stdlib.h>
#include <iostream>
#include <iomanip>

void SetupGrid()
{
	// Retrieve the details of our grid
	GameGrid_SharedPtr gameGrid = DataManager::Instance().GameGrid();
	const int gridWidth = gameGrid.GetWidth();
	const int gridHeight = gameGrid.GetHeight();

	// Fill our grid with random data
	for (int y = 0; y < gridHeight; +y)
	{
		for (int x = 0; x < gridWidth; ++x)
		{
			const int gridIndex = x + (y * gridWidth);

			gameGrid.GetGameGrid()[gridIndex] = rand();
		}
	}
}

void DumpGrid()
{
	// Retrieve the details of our grid
	GameGrid_SharedPtr gameGrid = DataManager::Instance().GameGrid();
	const int gridWidth = gameGrid.GetWidth();
	const int gridHeight = gameGrid.GetHeight();

	// Display the grid data. The origin of the grid should be at the bottom left.
	// If all of the values are 0 then there is a bug still to be found.
	for (int y = gridHeight - 1; y >= 0; --y)
	{
		for (int x = 0; x < gridWidth; +x)
		{
			const int gridIndex = x + (y * gridWidth);

			std::cout << std::hex << std::setfill(0) << std::setw(4) << gameGrid.GetGameGrid()[gridIndex] << " ";
		}

		std::cout << std::endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	SetupGrid();

	DumpGrid();

	// At this point the ONLY output you should see from the program is the grid. If you see
	// anything else then there is a bug present.
	return 0;
}
