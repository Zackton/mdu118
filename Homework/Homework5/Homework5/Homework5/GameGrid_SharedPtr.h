#pragma once

#include <set>

class GameGrid_WeakPtr;

class GameGrid_SharedPtr
{
public:
	static GameGrid_SharedPtr InvalidSharedPointer;

	GameGrid_SharedPtr();
	GameGrid_SharedPtr(int width, int height);
	GameGrid_SharedPtr(const GameGrid_SharedPtr& other);
	~GameGrid_SharedPtr();

	GameGrid_SharedPtr& operator=(const GameGrid_SharedPtr& other);

	bool IsValid() const 
	{ 
		return referenceCount && ((*referenceCount) > 0); 
	}

	int* GetGameGrid();

	int GetWidth() const
	{
		return gridWidth;
	}

	int GetHeight() const
	{
		return gridHeight;
	}

protected:
	void IncreaseRefCount();
	void DecreaseRefCount();

	// Helper methods to link and unlink weak pointers from this shared pointer
	void LinkWeakPointer(const GameGrid_WeakPtr& weakPointer) const;
	void UnlinkWeakPointer(const GameGrid_WeakPtr& weakPointer) const;

	// Helper method to invalidate all linked weak pointers
	void InvalidateLinkedWeakPointers();

	int* gameGrid;
	int gridWidth;
	int gridHeight;

	unsigned int* referenceCount;

	mutable std::set<const GameGrid_WeakPtr*>* linkedWeakPointers;

	friend class GameGrid_WeakPtr;
};

