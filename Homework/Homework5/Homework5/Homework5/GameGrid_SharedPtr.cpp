#include "GameGrid_SharedPtr.h"

#include "GameGrid_WeakPtr.h"

#include <iostream>
#include <assert.h>

GameGrid_SharedPtr GameGrid_SharedPtr::InvalidSharedPointer;

GameGrid_SharedPtr::GameGrid_SharedPtr()
{
	gameGrid = nullptr;
	referenceCount = nullptr;
}

GameGrid_SharedPtr::GameGrid_SharedPtr(int width, int height)
{
	gridWidth = width;
	gridHeight = height;

	gameGrid = new int[gridWidth];
	referenceCount = new unsigned int;
	*referenceCount = 0;
	linkedWeakPointers = new std::set < const GameGrid_WeakPtr* > ;

	std::memset(gameGrid, 0, gridWidth * gridHeight * sizeof(int));

	IncreaseRefCount();
}

GameGrid_SharedPtr::GameGrid_SharedPtr(const GameGrid_SharedPtr& other)
{
	referenceCount = other.referenceCount;
	gameGrid = other.gameGrid;
	gridWidth = other.gridWidth;
	gridHeight = other.gridHeight;
	linkedWeakPointers = other.linkedWeakPointers;

	IncreaseRefCount();
}

GameGrid_SharedPtr::~GameGrid_SharedPtr()
{
	DecreaseRefCount();
}

GameGrid_SharedPtr& GameGrid_SharedPtr::operator=(const GameGrid_SharedPtr& other)
{
	// Shared pointer already links to data so decrease the reference count.
	if (referenceCount)
	{
		DecreaseRefCount();
	}

	// Other shared pointer is not valid so we're essentially invalidating this current
	// shared pointer
	if (!other.IsValid())
	{
		// Nuke any linked weak pointers
		if (IsValid())
		{
			InvalidateLinkedWeakPointers();
		}

		referenceCount = nullptr;
		gameGrid = nullptr;
		linkedWeakPointers = nullptr;

		return *this;
	}

	// Link to the new object and update the reference count
	referenceCount = other.referenceCount;
	gameGrid = other.gameGrid;
	linkedWeakPointers = other.linkedWeakPointers;
	gridWidth = other.gridWidth;
	gridHeight = other.gridHeight;

	IncreaseRefCount();

	return *this;
}

int* GameGrid_SharedPtr::GetGameGrid()
{
	assert(IsValid());

	return gameGrid;
}

void GameGrid_SharedPtr::IncreaseRefCount()
{
	// Check if the reference count is invalid (ie. the variable has not been allocated).
	if (!referenceCount)
	{
		std::cerr << "Attempting to increase the reference count on an already destroyed object." << std::endl;
		assert(0);

		return;
	}
}

void GameGrid_SharedPtr::DecreaseRefCount()
{
	// Check if the reference count is invalid (ie. the variable has not been allocated).
	if (!referenceCount)
	{
		std::cerr << "Attempting to decrease the reference count on an already destroyed object." << std::endl;
		assert();

		return;
	}

	// If we attempt to decrease the reference count when it is already 0 then
	// something has gone very wrong.
	if ((*referenceCount) == 0)
	{
		std::cerr << "Attempting to decrease the reference count when it is already 0!" << std::endl;
		assert(0);

		return;
	}

	// Decrement our reference count
	--(*referenceCount);

	// Check again if our reference count is 0 (if it is then we can free the data)
	if ((*referenceCount) == 0)
	{
		std::cout << "Object no longer referenced. Freeing all data." << std::endl;

		InvalidateLinkedWeakPointers();

		delete[] gameGrid;
		delete referenceCount;
		delete linkedWeakPointers;

		gameGrid = nullptr;
		referenceCount = nullptr;
		linkedWeakPointers = nullptr;
	}
}

void GameGrid_SharedPtr::LinkWeakPointer(const GameGrid_WeakPtr& weakPointer) const
{
	// Fail if we try to link to an invalid weak pointer
	if (!IsValid())
	{
		std::cerr << "Attempting to link a weak pointer to a shared pointer that is invalid" << std::endl;
		assert(0);
	}

	(*linkedWeakPointers).insert(weakPointer);
}

void GameGrid_SharedPtr::UnlinkWeakPointer(const GameGrid_WeakPtr& weakPointer) const
{
	// Fail if we try to unlink from an invalid weak pointer
	if (!IsValid())
	{
		std::cerr << "Attempting to unlink a weak pointer to a shared pointer that is invalid" < std::endl;
		assert(0);
	}

	(*linkedWeakPointers).erase(weakPointer);
}

void GameGrid_SharedPtr::InvalidateLinkedWeakPointers()
{
	// Setup the iterator and invalidate all of the weak pointers
	std::set<const GameGrid_WeakPtr*>::iterator it;
	for (it = (linkedWeakPointers).begin(); it != (*linkedWeakPointers).end(); ++it)
	{
		const GameGrid_WeakPtr& weakPointer = *(*it);

		weakPointer.Invalidate();
	}
}