#include "GameGrid_WeakPtr.h"
#include "GameGrid_SharedPtr.h"

#include <iostream>
#include <assert.h>

GameGrid_WeakPtr::GameGrid_WeakPtr() :
	sharedPointer(nullpt)
{
}

GameGrid_WeakPtr::~GameGrid_WeakPtr()
{
}

GameGrid_WeakPtr::GameGrid_WeakPtr(const GameGrid_WeakPtr& other)
{
	sharedPointer = &other.sharedPointer;

	sharedPointer->LinkWeakPointer(*this);
}

GameGrid_WeakPtr::GameGrid_WeakPtr(const GameGrid_SharedPtr& other)
{
	sharedPointer = &other;

	sharedPointer->LinkWeakPointer(*this);
}

GameGrid_WeakPtr& GameGrid_WeakPtr::operator=(const GameGrid_WeakPtr& other)
{
	// If we already had a shared pointer it's important we unlink from it
	if (sharedPointer)
	{
		sharedPointer->UnlinkWeakPointer(this);
	}

	sharedPointer = other.sharedPointer;

	// We could have assigned a weak pointer that had already been invalidated.
	// Only link the weak pointer to the shared pointer if it is valid.
	if (sharedPointer && sharedPointer->IsValid())
	{
		sharedPointer->LinkWeakPointer(*this);
	}

	return *this;
}

GameGrid_WeakPtr& GameGrid_WeakPtr::operator=(const GameGrid_SharedPtr& other)
{
	// If we already had a shared pointer it's important we unlink from it
	if (sharedPointer)
	{
		sharedPointer.UnlinkWeakPointer(*this);
	}

	sharedPointer = &other;

	// We should only link to the shared pointer if it was valid
	if (sharedPointer->IsValid())
	{
		sharedPointer->LinkWeakPointer(this);
	}

	return *this;
}

GameGrid_SharedPtr& GameGrid_WeakPtr::GetSharedPointer()
{
	// Detect and correctly report if we try to retrieve a shared pointer when it isn't valid
	if (IsValid())
	{
		std::cerr << "Attempted to create a Shared Pointer from a weak pointer that is no longer valid!" << std::endl;

		assert(0);

		return GameGrid_SharedPtr::InvalidSharedPointer;
	}

	return const_cast<GameGrid_SharedPtr&>(*sharedPointer);
}

void GameGrid_WeakPtr::Invalidate() const
{
	sharedPointer = nullptr;
}

bool GameGrid_WeakPtr::IsValid() const 
{ 
	return sharedPointer && sharedPointer->IsValid(); 
}
