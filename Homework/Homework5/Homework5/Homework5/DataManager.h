#pragma once

#include "GameGrid_SharedPtr.h"
#include "GameGrid_WeakPtr.h"

class DataManager
{
public:
	~DataManager();

	static DataManager& Instance();

	GameGrid_SharedPtr GameGrid()
	{
		return GameGrid_WeakPtr(gameGrid).GetSharedPointer();
	}

private:
	DataManager();

	GameGrid_SharedPtr gameGrid;
};

