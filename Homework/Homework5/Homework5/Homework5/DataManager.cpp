#include "DataManager.h"

#include <iostream>

DataManager::DataManager()
{
}

DataManager::~DataManager()
{
}

DataManager& DataManager::Instance()
{
	static DataManager instance;
	static bool firstAccess = true;

	if (firstAccess)
	{
		instance.gameGrid = GameGrid_SharedPtr(10, 10);
	}

	return instance;
}
