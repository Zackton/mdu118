#pragma once

class GameGrid_SharedPtr;

class GameGrid_WeakPtr
{
public:
	GameGrid_WeakPtr();
	~GameGrid_WeakPtr();

	GameGrid_WeakPtr(const GameGrid_WeakPtr& other);
	GameGrid_WeakPtr(const GameGrid_SharedPtr& other);

	GameGrid_WeakPtr& operator=(const GameGrid_WeakPtr& other);
	GameGrid_WeakPtr& operator=(const GameGrid_SharedPtr& other);

	GameGrid_SharedPtr& GetSharedPointer();

	void Invalidate() const;
	bool IsValid() const;

protected:
	mutable const GameGrid_SharedPtr* sharedPointer;
};

