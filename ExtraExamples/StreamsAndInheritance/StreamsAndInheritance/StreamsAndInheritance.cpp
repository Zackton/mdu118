#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

// Basic Vector2 class
struct Vector2 {
	Vector2(float _x = 0, float _y = 0) :
		x(_x),
		y(_y)
	{

	}

	float x;
	float y;
};

// Setup the stream output operator for my vector 2 class
std::ostream& operator << (std::ostream& os, const Vector2& vec)
{
	return os << "(" << vec.x << "," << vec.y << ")";
}

// Setup the stream input operator for my vector 2 class
std::istream& operator >> (std::istream& is, Vector2& vec)
{
	// dummyChar is a throw away character that we use as a dumping ground for the parentheses and the comma
	char dummyChar;

	return is >> dummyChar
		>> vec.x
		>> dummyChar
		>> vec.y
		>> dummyChar;
}

// Basic shape class
class Shape {
public:
	Shape(const std::string& _name = "", const Vector2& _location = Vector2(), float _scale = 1.0f) :
		name(_name),
		location(_location),
		scale(_scale)
	{

	}

	virtual std::string ClassId() const { return "Shape"; }

public:
	std::string name;
	Vector2 location;
	float scale;
};

// Circle class based on the shape class
class Circle : public Shape {
public:
	Circle(const std::string& _name = "", const Vector2& _location = Vector2(), float _scale = 1.0f, float _radius = 1.0f) :
		Shape(_name, _location, _scale),
		radius(_radius)
	{

	}

	virtual std::string ClassId() const { return "Circle"; }

public:
	float radius;
};

// Setup the stream output operator for my shape class
std::ostream& operator << (std::ostream& os, const Shape& shape)
{
	return os << shape.name << ","
		<< shape.location << ","
		<< shape.scale;
}

// Setup the stream input operator for my shape class
std::istream& operator >> (std::istream& is, Shape& shape)
{
	// dummyChar is a throw away character that we use as a dumping ground for the parentheses and the comma
	char dummyChar;

	// Because the name is a string I can't use:
	//    is >> shape.name
	// If I did it would read the entire line.
	// Instead I use getline and tell it to stop when it hits a comma. The comma will also be consumed
	// so I don't need to read it into a dummyChar later before reading the location
	std::getline(is, shape.name, ',');

	return is >> shape.location
		>> dummyChar
		>> shape.scale;
}

// Setup the stream output operator for my shape class
std::ostream& operator << (std::ostream& os, const Circle& circle)
{
	return os << static_cast<const Shape&>(circle) << "," << circle.radius;
}

// Setup the stream input operator for my shape class
std::istream& operator >> (std::istream& is, Circle& circle)
{
	// dummyChar is a throw away character that we use as a dumping ground for the parentheses and the comma
	char dummyChar;

	return is >> static_cast<Shape&>(circle) 
		      >> dummyChar
		       >> circle.radius;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Setup a vector of pointers to a shape
	std::vector<Shape*> shapes;

	shapes.push_back(new Shape("Shape1", Vector2(1.0f, 3.4f), 1.0f));
	shapes.push_back(new Shape("Shape2", Vector2(11.0f, 1.4f), 1.0f));
	shapes.push_back(new Circle("Circle1", Vector2(2.0f, 3.7f), 1.0f, 5.0f));
	shapes.push_back(new Circle("Circle2", Vector2(1.6f, 9.4f), 1.0f, 10.0f));

	{ // Write out all of my shapes
		std::ofstream shapeCSVFile("shapes.csv");

		// Iterate over all of the shapes writing them to the CSV file
		for (std::vector<Shape*>::iterator shapeIt = shapes.begin(); shapeIt != shapes.end(); ++shapeIt)
		{
			Shape* shapePtr = *shapeIt;
			Shape& shape = *shapePtr;

			// Because the shape could be multiple types I need to output the class Id as well
			shapeCSVFile << shape.ClassId() << ",";

			// If the class Id is for a shape then instantiate a new shape
			if (shape.ClassId() == std::string("Shape")) {
				shapeCSVFile << static_cast<Shape&>(shape);
			} // Otherwise if the class id is for a circle then instantiate the circle
			else if (shape.ClassId() == std::string("Circle")) {
				shapeCSVFile << static_cast<Circle&>(shape);
			}

			shapeCSVFile << std::endl;
		}
	}

	{ // Read in all of my shapes
		std::ifstream shapeCSVFile("shapes.csv");

		std::vector<Shape*> loadedShapes;

		std::string csvLine;
		while (shapeCSVFile >> csvLine)
		{
			// Passing the file line to inputStream so we can deconstruct it
			std::istringstream inputStream(csvLine);

			Shape* shape;

			// Load the class id and remove the comma after it
			char dummyChar;
			std::string classId;
			std::getline(inputStream, classId, ',');

			// If the class Id is for a shape then instantiate a new shape
			if (classId == std::string("Shape")) {
				shape = new Shape();

				// Run our stream input 
				inputStream >> static_cast<Shape&>(*shape);
			} // Otherwise if the class id is for a circle then instantiate the circle
			else if (classId == std::string("Circle")) {
				shape = new Circle();

				// Run our stream input 
				inputStream >> static_cast<Circle&>(*shape);
			}

			loadedShapes.push_back(shape);
		}

		std::cout << "Loaded " << loadedShapes.size() << " shapes." << std::endl;

		// Cleanup my loaded shapes
		for (std::vector<Shape*>::iterator shapeIt = loadedShapes.begin(); shapeIt != loadedShapes.end(); ++shapeIt)
		{
			Shape* shapePtr = *shapeIt;

			delete shapePtr;
		}
	}

	// Cleanup my shapes
	for (std::vector<Shape*>::iterator shapeIt = shapes.begin(); shapeIt != shapes.end(); ++shapeIt)
	{
		Shape* shapePtr = *shapeIt;

		delete shapePtr;
	}

	return 0;
}

